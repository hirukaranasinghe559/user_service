package com.userservice.UserService.controllers;

import com.userservice.UserService.dto.OrderDTO;
import com.userservice.UserService.dto.UserDTO;
import com.userservice.UserService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;
    /*This method is used to return all the user data*/
    @GetMapping("/getAllDummyUsers")
    public List<UserDTO> getAllDummyUsers(){
        return Arrays.asList(
                new UserDTO("1", "Thushan", "24"),
                new UserDTO("2", "Nishani", "22")
        );
    }

    @GetMapping("/getAll")
    public List<UserDTO> getAllUsers(){
        return userService.getAllUsers();
    }

    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId(@PathVariable final String id){
        return userService.getOrdersByUserId(id);
    }

    @PostMapping(value ="/create-user", consumes = "application/json")
    public boolean createUser(@RequestBody Map<String, String> user){
        return userService.createUser(user);
    }

    @PutMapping(value = "/updateUserById/{id}", consumes = "application/json")
    public boolean updateUserById(@PathVariable final String id, @RequestBody Map<String, String> userDetails){
        return userService.updateUserById(id, userDetails);
    }

    @PostMapping(value = "/create-order", consumes = "application/json")
    public boolean createOrderByUserId(@RequestBody Map<String, String> order){
        return  userService.createOrderByUserId(order);
    }
}
