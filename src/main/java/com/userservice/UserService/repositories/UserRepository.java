package com.userservice.UserService.repositories;

import com.userservice.UserService.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Query("from UserEntity ue where ue.id =?1")
    UserEntity findUserEntityById(long id);
}
