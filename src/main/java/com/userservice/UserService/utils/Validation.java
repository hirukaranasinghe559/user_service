package com.userservice.UserService.utils;


import com.userservice.UserService.entities.UserEntity;

public class Validation {
    public static boolean validateInput(UserEntity userEntity){
        return Integer.parseInt(userEntity.getAge()) > 0 && Integer.parseInt(userEntity.getAge()) < 120 && !userEntity.getName().isEmpty();
    }
}
