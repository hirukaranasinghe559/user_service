package com.userservice.UserService.services;
import com.userservice.UserService.dto.OrderDTO;
import com.userservice.UserService.dto.UserDTO;
import com.userservice.UserService.entities.UserEntity;
import com.userservice.UserService.repositories.UserRepository;
import com.userservice.UserService.utils.Validation;
import org.aspectj.weaver.ast.Or;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Order;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Value("${order-service.base-url}")
    private String orderServiceBaseUrl;

    @Value("${order-service.order-url}")
    private String getOrderServiceOrderUrl;

    @Value("${order-service.create-order-url}")
    private String createOrderServiceUrl;

    @Autowired
    private RestTemplateBuilder restTemplate;

    @Autowired
    private UserRepository repository;

    public List<OrderDTO> getOrdersByUserId(String id){
        LOGGER.warn("Enter into getOrdersByUserId");
        List<OrderDTO> orders = null;
        try {
            orders = restTemplate.build().getForObject(
                    orderServiceBaseUrl.concat(getOrderServiceOrderUrl).concat("/"+ id),
                    List.class
            );
        }
        catch (Exception e){
            LOGGER.warn("Exception in UserService -> getOrdersByUserId()" + e);
        }

        return orders;
    }

    public List<UserDTO> getAllUsers() {
        List<UserDTO> users = null;
        LOGGER.warn("*-----------Enter into getAllUsers---------*");
        users = repository.findAll()
                .stream()
                .map(userEntity -> new UserDTO(
                        Long.toString(userEntity.getId()),
                        userEntity.getName(),
                        userEntity.getAge()
                )).collect(Collectors.toList());

        return users;
    }

    /*createUser method creates a user by using the request body
     if the data in the request body is valid. */
    public boolean createUser(Map<String,String> user){
        try {
            UserEntity userEntity = new UserEntity(user.get("name"), user.get("age"));
            LOGGER.info("Username: "+ user.get("name") + ", Age: " + user.get("age"));
            if (Validation.validateInput(userEntity)){

                repository.save(userEntity);
                return true;
            }
            LOGGER.warn("*******************Validation Failed*******************");
        }catch (Exception e){
            LOGGER.warn("****************** Exception Occurred ******************" + e);
        }
        return false;
    }

    /*updateUserById method checks if the user id exists and if the user id exist,
    request body data is used to update the record. */
    public boolean updateUserById(String userId, Map<String,String> userDetails){
        try {
            if(repository.existsById(Long.parseLong(userId))){
                UserEntity userEntity = new UserEntity(
                        Long.parseLong(userId),
                        userDetails.get("name"),
                        userDetails.get("age")
                );
                LOGGER.info("Updated User Entity Created.");
                repository.save(userEntity);
                return true;
            }
            else {
                LOGGER.warn("User Id Doesn't Exist.");
                return false;
            }
        }
        catch (Exception e){
            LOGGER.warn("Exception in UserService -> updateUserById()" + e);
        }
        return false;
    }

    /* This method checks if the user exists and if existed, an order is created*/
    public boolean createOrderByUserId(Map<String,String> order){
        try {
            if (repository.existsById(Long.parseLong(order.get("user_id")))){
                boolean createOrder;
                OrderDTO orderDTO = new OrderDTO(
                        order.get("id"),
                        order.get("order_id"),
                        order.get("user_id")
                );
                createOrder = Boolean.TRUE.equals(restTemplate.build().postForObject(
                        orderServiceBaseUrl.concat(createOrderServiceUrl), orderDTO, boolean.class
                ));
                LOGGER.info("Order Object Created: " + orderDTO);

                return true;
            }
            else {
                LOGGER.info("User Id Not found");
                return false;
            }
        }
        catch (Exception e){
            LOGGER.warn("****************** Exception Occurred ******************" + e);
        }
        return false;
    }
}