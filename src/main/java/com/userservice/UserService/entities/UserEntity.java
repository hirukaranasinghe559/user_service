package com.userservice.UserService.entities;

import javax.persistence.*;

@Entity
@Table(name = "user_table")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String age;

    public UserEntity() {
    }

    public UserEntity(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public UserEntity(Long id, String name, String age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public Long getId() {return id;}

    public void setId(long id) {this.id = id;}

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public String getAge() {return age;}

    public void setAge(String age) {this.age = age;}
}
